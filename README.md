# Ресурсы

## Frontend

* Angular.js
** https://angular.io/
* Mithril.js
** https://mithril.js.org/
* React.js
** https://react.dev/
* Vue.js
** https://vuejs.org/

## Backend

### Javascript

* Node.js

### Python

* Flask

## Fullstack

### Stack #1

* Mithril.js
** https://mithril.js.org/
** https://unpkg.com/mithril/mithril.js
** https://unpkg.com/mithril/mithril.min.js
* Bootstrap - только CSS файл
** https://getbootstrap.com/
** https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css
* Flask
** https://flask.palletsprojects.com/en/latest/
* SQLAlchemy
** https://www.sqlalchemy.org/
